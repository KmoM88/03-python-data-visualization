"""
Project for Week 4 of "Python Data Visualization".
Unify data via common country codes.

Be sure to read the project description page for further information
about the expected behavior of the program.
"""

import csv
import math
import pygal


def build_country_code_converter(codeinfo):
    """
    Inputs:
      codeinfo      - A country code information dictionary

    Output:
      A dictionary whose keys are plot country codes and values
      are world bank country codes, where the code fields in the
      code file are specified in codeinfo.
    """
    # pygal_countries = pygal.maps.world.COUNTRIES
    # print(list(pygal_countries.values()))
    ret_dict = {}
    with open(codeinfo["codefile"]) as csv_file:
        csv_dictreader = csv.DictReader(csv_file, delimiter=codeinfo["separator"], quotechar=codeinfo[str("quote")])
        # ext_dict = [tuple()]
        # result_ext_dict = {}
        for row in csv_dictreader:
            if (row[codeinfo["plot_codes"]].lower() != "" and row[codeinfo["data_codes"]].lower() !=""):
                ret_dict[row[codeinfo["plot_codes"]].lower()] = row[codeinfo["data_codes"]].lower()
            # print(row[codeinfo["plot_codes"]].lower())
            # print(row[codeinfo["data_codes"]].lower())
        #         row["name"]
    return ret_dict


def reconcile_countries_by_code(codeinfo, plot_countries, gdp_countries):
    """
    Inputs:
      codeinfo       - A country code information dictionary
      plot_countries - Dictionary whose keys are plot library country codes
                       and values are the corresponding country name
      gdp_countries  - Dictionary whose keys are country codes used in GDP data

    Output:
      A tuple containing a dictionary and a set.  The dictionary maps
      country codes from plot_countries to country codes from
      gdp_countries.  The set contains the country codes from
      plot_countries that did not have a country with a corresponding
      code in gdp_countries.

      Note that all codes should be compared in a case-insensitive
      way.  However, the returned dictionary and set should include
      the codes with the exact same case as they have in
      plot_countries and gdp_countries.
    """
    ret_dict = {}
    ret_set = set()
    with open(codeinfo["codefile"]) as csv_file:
        csv_dictreader = csv.DictReader(csv_file, delimiter=codeinfo["separator"], quotechar=codeinfo[str("quote")])
        ext_dict = [tuple()]
        result_ext_dict = {}
        key_list = list(plot_countries.keys())
        val_list = list(plot_countries.values())
        for key in gdp_countries:
            if gdp_countries[key]["Country Name"] in plot_countries.values():
                # print(val_list.index(gdp_countries[key]["Country Name"]))
                # print(key_list[val_list.index(gdp_countries[key]["Country Name"])])
                ret_dict[key_list[val_list.index(gdp_countries[key]["Country Name"])]] = gdp_countries[key]["Country Code"]
        for key in plot_countries:
            if key not in list(ret_dict.keys()):
                ret_set.add(key)
            # print(row[codeinfo["plot_codes"]].lower())
            # print(row[codeinfo["data_codes"]].lower())
        #         row["name"]
    return ret_dict, ret_set


def build_map_dict_by_code(gdpinfo, codeinfo, plot_countries, year):
    """
    Inputs:
      gdpinfo        - A GDP information dictionary
      codeinfo       - A country code information dictionary
      plot_countries - Dictionary mapping plot library country codes to country names
      year           - String year for which to create GDP mapping

    Output:
      A tuple containing a dictionary and two sets.  The dictionary
      maps country codes from plot_countries to the log (base 10) of
      the GDP value for that country in the specified year.  The first
      set contains the country codes from plot_countries that were not
      found in the GDP data file.  The second set contains the country
      codes from plot_countries that were found in the GDP data file, but
      have no GDP data for the specified year.
    """
    
    return {}, set(), set()

gdpinfo = {
    "gdpfile": "isp_gdp.csv",
    "separator": ",",
    "quote": '"',
    "min_year": 1960,
    "max_year": 2015,
    "country_name": "Country Name",
    "country_code": "Country Code"
}

codeinfo = {
    "codefile": "isp_country_codes.csv",
    "separator": ",",
    "quote": '"',
    "plot_codes": "ISO3166-1-Alpha-2",
    "data_codes": "ISO3166-1-Alpha-3"
}
plot_countries = {'pr': 'Puerto Rico', 'no': 'Norway', 'us': 'United States'}
gdp_countries = {'USA': {'Country Name': 'United States', 'Country Code': 'USA'}, 'NOR': {'Country Name': 'Norway', 'Country Code': 'NOR'}, 'PRI': {'Country Name': 'Puerto Rico', 'Country Code': 'PRI'}}
# # print(build_country_code_converter(codeinfo))
# # print(len(build_country_code_converter(codeinfo)))
# print(reconcile_countries_by_code(codeinfo, plot_countries, gdp_countries))
print(build_map_dict_by_code(gdpinfo, codeinfo, plot_countries, 2000))

def render_world_map(gdpinfo, codeinfo, plot_countries, year, map_file):
    """
    Inputs:
      gdpinfo        - A GDP information dictionary
      codeinfo       - A country code information dictionary
      plot_countries - Dictionary mapping plot library country codes to country names
      year           - String year of data
      map_file       - String that is the output map file name

    Output:
      Returns None.

    Action:
      Creates a world map plot of the GDP data in gdp_mapping and outputs
      it to a file named by svg_filename.
    """
    return


def test_render_world_map():
    """
    Test the project code for several years
    """
    gdpinfo = {
        "gdpfile": "isp_gdp.csv",
        "separator": ",",
        "quote": '"',
        "min_year": 1960,
        "max_year": 2015,
        "country_name": "Country Name",
        "country_code": "Country Code"
    }

    codeinfo = {
        "codefile": "isp_country_codes.csv",
        "separator": ",",
        "quote": '"',
        "plot_codes": "ISO3166-1-Alpha-2",
        "data_codes": "ISO3166-1-Alpha-3"
    }

    # Get pygal country code map
    pygal_countries = pygal.maps.world.COUNTRIES

    # 1960
    render_world_map(gdpinfo, codeinfo, pygal_countries, "1960", "isp_gdp_world_code_1960.svg")

    # 1980
    render_world_map(gdpinfo, codeinfo, pygal_countries, "1980", "isp_gdp_world_code_1980.svg")

    # 2000
    render_world_map(gdpinfo, codeinfo, pygal_countries, "2000", "isp_gdp_world_code_2000.svg")

    # 2010
    render_world_map(gdpinfo, codeinfo, pygal_countries, "2010", "isp_gdp_world_code_2010.svg")


# Make sure the following call to test_render_world_map is commented
# out when submitting to OwlTest/CourseraTest.

# test_render_world_map()