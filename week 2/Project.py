"""
Project for Week 2 of "Python Data Visualization".
Read World Bank GDP data and create some basic XY plots.

Be sure to read the project description page for further information
about the expected behavior of the program.
"""

import csv
import pygal

def draw_xy(plot_dict, gdpinfo):
    """
    Draw xy plot with given x and y values.
    """
    data_dict = {}
    xyplot = pygal.XY(height=400)
    xyplot.title = "Plot of GDP for select countries spanning " + str(gdpinfo["min_year"]) + " t " + str(gdpinfo["max_year"])
    for key in plot_dict:
        coords = plot_dict[key]
        # print(coords)
        xyplot.add(key, coords)
    # coords = [(xval, yval) for xval, yval in zip(xvals, yvals)]
    # xyplot.add("Data", coords)
    xyplot.render_in_browser()
    return xyplot

def read_csv_as_nested_dict(filename, keyfield, separator, quote):
    """
    Inputs:
      filename  - Name of CSV file
      keyfield  - Field to use as key for rows
      separator - Character that separates fields
      quote     - Character used to optionally quote fields

    Output:
      Returns a dictionary of dictionaries where the outer dictionary
      maps the value in the key_field to the corresponding row in the
      CSV file.  The inner dictionaries map the field names to the
      field values for that row.
    """
    with open(filename) as csv_file:
        csv_dictreader = csv.DictReader(csv_file, delimiter=separator, quotechar=str(quote))
        ext_dict = [tuple()]
        result_ext_dict = {}
        for row in csv_dictreader:
            for key in list(row.keys()):
                if key == keyfield:
                    ext_dict.append((row[key], row))
        for t in ext_dict:
            if t == ():
                print("XXX")
            else:
                if (t[0] in list(result_ext_dict.keys())):
                    print(t[0] in list(result_ext_dict.keys()))
                    print("Repeated Key")
                result_ext_dict[t[0]] = t[1]
    return result_ext_dict

def build_plot_values(gdpinfo, gdpdata):
    """
    Inputs:
      gdpinfo - GDP data information dictionary
      gdpdata - A single country's GDP stored in a dictionary whose
                keys are strings indicating a year and whose values
                are strings indicating the country's corresponding GDP
                for that year.

    Output:
      Returns a list of tuples of the form (year, GDP) for the years
      between "min_year" and "max_year", inclusive, from gdpinfo that
      exist in gdpdata.  The year will be an integer and the GDP will
      be a float.
    """
    ret_tuple = [tuple()]
    # print(list(gdpdata.keys()))
    for x in range(gdpinfo["min_year"],gdpinfo["max_year"] + 1):
        if str(x) in list(gdpdata.keys()) and (gdpdata[str(x)] != ""):
            ret_tuple.append((x, float(gdpdata[str(x)])))
    # print(ret_tuple[1:])
    return list(ret_tuple[1:])

def build_plot_dict(gdpinfo, country_list):
    """
    Inputs:
      gdpinfo      - GDP data information dictionary
      country_list - List of strings that are country names

    Output:
      Returns a dictionary whose keys are the country names in
      country_list and whose values are lists of XY plot values
      computed from the CSV file described by gdpinfo.

      Countries from country_list that do not appear in the
      CSV file should still be in the output dictionary, but
      with an empty XY plot value list.
    """
    gdpdata = read_csv_as_nested_dict(gdpinfo["gdpfile"], gdpinfo["country_name"], gdpinfo["separator"], gdpinfo["quote"])
    dict_ret = {}
    for elem in country_list:
        try:
          dict_ret[elem] = build_plot_values(gdpinfo, gdpdata[elem])
        except KeyError:
          dict_ret[elem] = []
    return dict_ret


def render_xy_plot(gdpinfo, country_list, plot_file):
    """
    Inputs:
      gdpinfo      - GDP data information dictionary
      country_list - List of strings that are country names
      plot_file    - String that is the output plot file name

    Output:
      Returns None.

    Action:
      Creates an SVG image of an XY plot for the GDP data
      specified by gdpinfo for the countries in country_list.
      The image will be stored in a file named by plot_file.
    """
    plot_dict = build_plot_dict(gdpinfo, country_list)
    draw_xy(plot_dict, gdpinfo)
    # for key in plot_dict:


# gdpinfo = {
#         "gdpfile": "isp_gdp.csv",         # Name of the GDP CSV file
#         "separator": ",",                 # Separator character in CSV file
#         "quote": '"',                     # Quote character in CSV file
#         "min_year": 1980,                 # Oldest year of GDP data in CSV file
#         "max_year": 2000,                 # Latest year of GDP data in CSV file
#         "country_name": "Country Name",   # Country name field name
#         "country_code": "Country Code"    # Country code field name
#     }

# country_list = ["United Kingdom", "United States"]
# print(build_plot_dict(gdpinfo, country_list))

# print(gdpdata["Argentina"])
# # print(len(gdpdata["Argentina"]))
# print(build_plot_values(gdpinfo, gdpdata["Argentina"]))
# render_xy_plot(gdpinfo, country_list, "Plot.svg")

# xvals = [0, 1, 3, 5, 6, 7, 9, 11, 12, 15]
# yvals = [4, 3, 1, 2, 2, 4, 5, 2, 1, 4]

# draw_xy("My XY Plot", xvals, yvals)

def test_render_xy_plot():
    """
    Code to exercise render_xy_plot and generate plots from
    actual GDP data.
    """
    gdpinfo = {
        "gdpfile": "isp_gdp.csv",
        "separator": ",",
        "quote": '"',
        "min_year": 1960,
        "max_year": 2015,
        "country_name": "Country Name",
        "country_code": "Country Code"
    }

    render_xy_plot(gdpinfo, [], "isp_gdp_xy_none.svg")
    render_xy_plot(gdpinfo, ["China"], "isp_gdp_xy_china.svg")
    render_xy_plot(gdpinfo, ["United Kingdom", "United States"],
                   "isp_gdp_xy_uk+usa.svg")



# Make sure the following call to test_render_xy_plot is commented out
# when submitting to OwlTest/CourseraTest.

# test_render_xy_plot()
