"""
Project for Week 3 of "Python Data Visualization".
Unify data via common country name.

Be sure to read the project description page for further information
about the expected behavior of the program.
"""

import csv
import math
import pygal

def read_csv_as_nested_dict(filename, keyfield, separator, quote):
    """
    Inputs:
      filename  - Name of CSV file
      keyfield  - Field to use as key for rows
      separator - Character that separates fields
      quote     - Character used to optionally quote fields

    Output:
      Returns a dictionary of dictionaries where the outer dictionary
      maps the value in the key_field to the corresponding row in the
      CSV file.  The inner dictionaries map the field names to the
      field values for that row.
    """
    with open(filename) as csv_file:
        csv_dictreader = csv.DictReader(csv_file, delimiter=separator, quotechar=str(quote))
        ext_dict = [tuple()]
        result_ext_dict = {}
        for row in csv_dictreader:
            for key in list(row.keys()):
                if key == keyfield:
                    ext_dict.append((row[key], row))
        for t in ext_dict:
            if t == ():
                print("XXX")
            else:
                if (t[0] in list(result_ext_dict.keys())):
                    print(t[0] in list(result_ext_dict.keys()))
                    print("Repeated Key")
                result_ext_dict[t[0]] = t[1]
    return result_ext_dict

def build_plot_values(gdpinfo, gdpdata):
    """
    Inputs:
      gdpinfo - GDP data information dictionary
      gdpdata - A single country's GDP stored in a dictionary whose
                keys are strings indicating a year and whose values
                are strings indicating the country's corresponding GDP
                for that year.

    Output:
      Returns a list of tuples of the form (year, GDP) for the years
      between "min_year" and "max_year", inclusive, from gdpinfo that
      exist in gdpdata.  The year will be an integer and the GDP will
      be a float.
    """
    ret_tuple = [tuple()]
    # print(list(gdpdata.keys()))
    for x in range(gdpinfo["min_year"],gdpinfo["max_year"] + 1):
        if str(x) in list(gdpdata.keys()) and (gdpdata[str(x)] != ""):
            ret_tuple.append((x, float(gdpdata[str(x)])))
    # print(ret_tuple[1:])
    return list(ret_tuple[1:])

def build_plot_dict(gdpinfo, country_list):
    """
    Inputs:
      gdpinfo      - GDP data information dictionary
      country_list - List of strings that are country names

    Output:
      Returns a dictionary whose keys are the country names in
      country_list and whose values are lists of XY plot values
      computed from the CSV file described by gdpinfo.

      Countries from country_list that do not appear in the
      CSV file should still be in the output dictionary, but
      with an empty XY plot value list.
    """
    gdpdata = read_csv_as_nested_dict(gdpinfo["gdpfile"], gdpinfo["country_name"], gdpinfo["separator"], gdpinfo["quote"])
    dict_ret = {}
    for elem in country_list:
        try:
          dict_ret[elem] = build_plot_values(gdpinfo, gdpdata[elem])
        except KeyError:
          dict_ret[elem] = []
    return dict_ret

def build_plot_data(gdpinfo, country_list):
    """
    Inputs:
      gdpinfo      - GDP data information dictionary
      country_list - List of strings that are country names

    Output:
      Returns a dictionary whose keys are the country names in
      country_list and whose values are lists of XY plot values
      computed from the CSV file described by gdpinfo.

      Countries from country_list that do not appear in the
      CSV file should still be in the output dictionary, but
      with an empty XY plot value list.
    """
    gdpdata = read_csv_as_nested_dict(gdpinfo["gdpfile"], gdpinfo["country_name"], gdpinfo["separator"], gdpinfo["quote"])
    dict_ret = {}
    for elem in country_list:
        try:
          dict_ret[elem] = build_plot_values(gdpinfo, gdpdata[elem])
        except KeyError:
          dict_ret[elem] = [(1, 1)]
    return dict_ret

def reconcile_countries_by_name(plot_countries, gdp_countries):
    """
    Inputs:
      plot_countries - Dictionary whose keys are plot library country codes
                       and values are the corresponding country name
      gdp_countries  - Dictionary whose keys are country names used in GDP data

    Output:
      A tuple containing a dictionary and a set.  The dictionary maps
      country codes from plot_countries to country names from
      gdp_countries The set contains the country codes from
      plot_countries that were not found in gdp_countries.
    """
    dict_ret = {}
    set_ret = set()
    key_plot_countries = list(plot_countries.keys())
    # print(plot_countries)
    for country in plot_countries:
        # print(list(gdp_countries.keys()))
        if plot_countries[country] in list(gdp_countries.keys()):
            # print(country)
            # print(plot_countries[country])
            dict_ret[country] = plot_countries[country]
        else:
            set_ret.add(country)
            # print("Está")
    return dict_ret, set_ret


# print(reconcile_countries_by_name({'pr': 'Puerto Rico', 'no': 'Norway', 'us': 'United States'}, {'United States': {'Country Name': 'United States', 'Country Code': 'USA'}, 'Norway': {'Country Name': 'Norway', 'Country Code': 'NOR'}}))


def build_map_dict_by_name(gdpinfo, plot_countries, year):
    """
    Inputs:
      gdpinfo        - A GDP information dictionary
      plot_countries - Dictionary whose keys are plot library country codes
                       and values are the corresponding country name
      year           - String year to create GDP mapping for

    Output:
      A tuple containing a dictionary and two sets.  The dictionary
      maps country codes from plot_countries to the log (base 10) of
      the GDP value for that country in the specified year.  The first
      set contains the country codes from plot_countries that were not
      found in the GDP data file.  The second set contains the country
      codes from plot_countries that were found in the GDP data file, but
      have no GDP data for the specified year.
    """
    keys, values = plot_countries.keys(), plot_countries.values()
    reverse_dict = dict(zip(values, keys))
    # print(reverse_dict)
    dict_ret = {}
    set1_ret = set()
    set2_ret = set()
    country_list = []
    gdpinfo2 = dict(gdpinfo)
    gdpinfo2["min_year"] = int(year)
    gdpinfo2["max_year"] = int(year)
    for country in plot_countries:
        country_list.append(plot_countries[country])
    plot_info = build_plot_data(gdpinfo2, country_list)
    # print(plot_info)
    for country in plot_info:
        if plot_info[country] == [(1, 1)]:
            set1_ret.add(reverse_dict[country])
        elif plot_info[country] != []:
            dict_ret[reverse_dict[country]] = math.log10(plot_info[country][0][1])
        else:
            set2_ret.add(reverse_dict[country])
    # print(dict_ret)
    # print(set1_ret)
    # print(set2_ret)
    return dict_ret, set1_ret, set2_ret


def render_world_map(gdpinfo, plot_countries, year, map_file):
    """
    Inputs:
      gdpinfo        - A GDP information dictionary
      plot_countries - Dictionary whose keys are plot library country codes
                       and values are the corresponding country name
      year           - String year to create GDP mapping for
      map_file       - Name of output file to create

    Output:
      Returns None.

    Action:
      Creates a world map plot of the GDP data for the given year and
      writes it to a file named by map_file.
    """
    [country_dict, missing_country_set, missing_data_set] = build_map_dict_by_name(gdpinfo, plot_countries, year)
    # print(country_dict)
    # print(missing_country_set)
    # print(missing_data_set)
    worldmap_chart = pygal.maps.world.World()
    worldmap_chart.title = 'GDP by country for ' + str(year) + ' (log scale), unified by common country NAME'
    worldmap_chart.add('GDP for ' + str(year), country_dict)
    worldmap_chart.add('Missing', missing_country_set)
    worldmap_chart.add('no GDP data', missing_data_set)
    worldmap_chart.render_in_browser()
    return

# gdpinfo = {
#     "gdpfile": "isp_gdp.csv",
#     "separator": ",",
#     "quote": '"',
#     "min_year": 1960,
#     "max_year": 2015,
#     "country_name": "Country Name",
#     "country_code": "Country Code"
# }
# plot_countries = {"ar": "Argentina" ,"be": "Be0nin" ,"sw": "Sweden" }
# build_map_dict_by_name(gdpinfo, plot_countries, 1960)

def test_render_world_map():
    """
    Test the project code for several years.
    """
    gdpinfo = {
        "gdpfile": "isp_gdp.csv",
        "separator": ",",
        "quote": '"',
        "min_year": 1960,
        "max_year": 2015,
        "country_name": "Country Name",
        "country_code": "Country Code"
    }

    # Get pygal country code map
    pygal_countries = pygal.maps.world.COUNTRIES

    # 1960
    render_world_map(gdpinfo, pygal_countries, "1960",
                     "isp_gdp_world_name_1960.svg")

    # 1980
    render_world_map(gdpinfo, pygal_countries, "1980",
                     "isp_gdp_world_name_1980.svg")

    # 2000
    render_world_map(gdpinfo, pygal_countries, "2000",
                     "isp_gdp_world_name_2000.svg")

    # 2010
    render_world_map(gdpinfo, pygal_countries, "2010",
                     "isp_gdp_world_name_2010.svg")


# Make sure the following call to test_render_world_map is commented
# out when submitting to OwlTest/CourseraTest.

# test_render_world_map()
